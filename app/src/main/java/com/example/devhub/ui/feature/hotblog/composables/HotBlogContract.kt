package com.example.devhub.ui.feature.hotblog.composables

import com.example.devhub.ui.base.ViewEvent
import com.example.devhub.ui.base.ViewSideEffect
import com.example.devhub.ui.base.ViewState
import com.example.devhub.ui.feature.hotblog.Article

class HotBlogContract {

    sealed class Event : ViewEvent {
        data class OnItemClicked(val article: Article) : Event()
    }

    data class State(
        val articles: List<Article>,
        val isLoading: Boolean,
        val isError: Boolean
    ) : ViewState

    sealed class Effect : ViewSideEffect {
        data object DataWasLoaded : Effect()
        sealed class Navigation : Effect() {
            data class ToArticle(val articleId: String): Navigation()
        }
    }
}