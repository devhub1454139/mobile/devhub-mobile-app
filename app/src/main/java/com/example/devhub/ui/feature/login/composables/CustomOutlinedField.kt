package com.example.devhub.ui.feature.login.composables

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusModifier
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import com.example.devhub.R
import com.example.devhub.ui.theme.DarkerGreen
import com.example.devhub.ui.theme.LightGreen
import com.example.devhub.ui.theme.TextColor
import com.example.devhub.ui.theme.White

@Composable
fun CustomOutlinedField(
    modifier: Modifier = Modifier,
    text: String,
    placeholder: String?,
    isPassword: Boolean = false,
    onTextChange: (String) -> Unit,
    onTextClear: () -> Unit = {},
) {
    var isPasswordVisible by rememberSaveable {
        mutableStateOf(false)
    }

    val isDark = isSystemInDarkTheme()

    val focusRegister = FocusRequester()
    var focusedAlphaColor by remember {
        mutableStateOf(0.0f)
    }

    OutlinedTextField(
        modifier = modifier
            .fillMaxWidth()
            .focusRequester(focusRegister)
            .onFocusChanged {
                focusedAlphaColor = if (it.isFocused) 0.15f else 0.0f
            },
        value = text,
        onValueChange = onTextChange,
        placeholder = { placeholder?.let { Text(text = it) } },
        shape = RoundedCornerShape(12.dp),
        trailingIcon = {
            AnimatedVisibility(visible = text.isNotEmpty()) {
                if (isPassword) {
                    IconButton(
                        onClick = {
                            isPasswordVisible = !isPasswordVisible
                        }) {
                        Icon(
                            painter = painterResource(id = if(isPasswordVisible) R.drawable.eye else R.drawable.eye_off),
                            contentDescription = "eye"
                        )
                    }
                } else {
                    IconButton(onClick = onTextClear) {
                        Icon(imageVector = Icons.Default.Clear, contentDescription = "clear")
                    }
                }
            }
        },
        visualTransformation = if (!isPasswordVisible && isPassword) PasswordVisualTransformation() else VisualTransformation.None,
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
        colors = TextFieldDefaults.outlinedTextFieldColors(
            textColor = if(isDark) White else TextColor,
            cursorColor = if(isDark) LightGreen else DarkerGreen,
            trailingIconColor = if(isDark) LightGreen else TextColor,
            backgroundColor = if(isDark) LightGreen.copy(focusedAlphaColor) else DarkerGreen.copy(focusedAlphaColor),
            placeholderColor = if(isDark) LightGreen else DarkerGreen,
            unfocusedBorderColor = if(isDark) LightGreen else DarkerGreen,
            focusedBorderColor = if(isDark) LightGreen else DarkerGreen
        ),
        maxLines = 1
    )
}