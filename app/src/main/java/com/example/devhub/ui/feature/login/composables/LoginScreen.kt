package com.example.devhub.ui.feature.login.composables

import android.annotation.SuppressLint
import androidx.compose.foundation.clickable
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.devhub.R
import com.example.devhub.ui.base.SIDE_EFFECT_KEY
import com.example.devhub.ui.feature.login.LoginContract
import com.example.devhub.ui.theme.DarkerGreen
import com.example.devhub.ui.theme.LightGreen
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun LoginScreen(
    state: LoginContract.State,
    effectFlow: Flow<LoginContract.Effect>?,
    onEventSent: (event: LoginContract.Event) -> Unit,
    onNavigationRequest: (LoginContract.Effect.Navigation) -> Unit
) {
    LaunchedEffect(SIDE_EFFECT_KEY) {
        effectFlow?.onEach { effect ->
            when (effect) {
                LoginContract.Effect.Navigation.ToMain -> {
                    onNavigationRequest(LoginContract.Effect.Navigation.ToMain)
                }
                LoginContract.Effect.Navigation.ToSighUp -> {
                    onNavigationRequest(LoginContract.Effect.Navigation.ToSighUp)
                }
            }
        }?.collect()
    }

    val isDark = isSystemInDarkTheme()

    Scaffold {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(horizontal = 20.dp)
                .navigationBarsPadding()
                .statusBarsPadding(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = stringResource(R.string.login_main_label),
                fontSize = 32.sp,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center
            )
            Spacer(modifier = Modifier.height(96.dp))
            CustomOutlinedField(
                text = state.email,
                placeholder = stringResource(R.string.textField_label_email),
                onTextChange = { onEventSent(LoginContract.Event.OnEmailChange(it)) },
                onTextClear = { onEventSent(LoginContract.Event.OnEmailChange("")) }
            )
            Spacer(modifier = Modifier.height(16.dp))
            CustomOutlinedField(
                text = state.password,
                placeholder = stringResource(R.string.textField_label_password),
                isPassword = true,
                onTextChange = { onEventSent(LoginContract.Event.OnPasswordChange(it)) },
            )
            Spacer(modifier = Modifier.height(80.dp))
            CustomButton(
                text = stringResource(R.string.btn_label_login),
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                onEventSent(LoginContract.Event.OnMainButtonClicked)
            }
            Spacer(modifier = Modifier.height(24.dp))
            Row(
                horizontalArrangement = Arrangement.spacedBy(4.dp)
            ) {
                Text(text = stringResource(R.string.haven_t_registered_yet))
                Text(
                    text = "SignUp",
                    modifier = Modifier
                        .clickable(onClick = { onEventSent(LoginContract.Event.OnSignUpButtonClicked) }),
                    color = if (isDark) LightGreen else DarkerGreen,
                    fontWeight = FontWeight.Bold
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewLoginScreen() {
    LoginScreen(
        state = LoginContract.State(
            email = "фывфывваенпгршорнпавпрапвпрорсрсрсрфывфывфывфыв",
            password = "123"
        ),
        effectFlow = null,
        onEventSent = {},
        onNavigationRequest = {}
    )
}