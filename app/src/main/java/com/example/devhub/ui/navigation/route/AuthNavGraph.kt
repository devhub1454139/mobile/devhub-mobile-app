package com.example.devhub.ui.navigation.route

import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import com.example.devhub.ui.feature.login.LoginContract
import com.example.devhub.ui.feature.login.composables.LoginScreen
import com.example.devhub.ui.feature.login.LoginViewModel
import com.example.devhub.ui.feature.singup.SignUpContract
import com.example.devhub.ui.feature.singup.SignUpViewModel
import com.example.devhub.ui.feature.singup.composables.SignUpScreen
import com.example.devhub.util.Graph

fun NavGraphBuilder.authNavGraph(
    navController: NavHostController
) {
    navigation(
        startDestination = AuthScreen.Login.route,
        route = Graph.AUTH_GRAPH_ROUTE
    ) {
        composable(AuthScreen.Login.route) {
            val viewModel = hiltViewModel<LoginViewModel>()
            LoginScreen(
                state = viewModel.viewState.value,
                effectFlow = viewModel.effect,
                onEventSent = { event -> viewModel.setEvent(event) },
                onNavigationRequest = { navigationEffect ->
                    if (navigationEffect is LoginContract.Effect.Navigation.ToSighUp) {
                        navController.navigate(AuthScreen.SingUp.route)
                    }
                    if (navigationEffect is LoginContract.Effect.Navigation.ToMain) {
                        navController.popBackStack()
                        navController.navigate(Graph.HOME_GRAPH_ROUTE)
                    }
                }
            )
        }
        composable(AuthScreen.SingUp.route) {
            val viewModel = hiltViewModel<SignUpViewModel>()
            SignUpScreen(
                state = viewModel.viewState.value,
                effectFlow = viewModel.effect,
                onEventSent = { event -> viewModel.setEvent(event) },
                onNavigationRequest = { navigationEffect ->
                    if (navigationEffect is SignUpContract.Effect.Navigation.Back) {
                        navController.popBackStack()
                    }
                    if (navigationEffect is SignUpContract.Effect.Navigation.ToMain) {
                        navController.navigate(Graph.HOME_GRAPH_ROUTE) {
                            popUpTo(Graph.AUTH_GRAPH_ROUTE) {
                                inclusive = true
                            }
                        }
                    }
                }
            )
        }
    }
}

private sealed class AuthScreen(val route: String) {
    data object Login : AuthScreen("login")
    data object SingUp : AuthScreen("singup")
}