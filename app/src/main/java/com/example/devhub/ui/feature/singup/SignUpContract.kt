package com.example.devhub.ui.feature.singup

import com.example.devhub.ui.base.ViewEvent
import com.example.devhub.ui.base.ViewSideEffect
import com.example.devhub.ui.base.ViewState

class SignUpContract {

    sealed class Event : ViewEvent {
        data class OnUsernameChange(val username: String) : Event()
        data class OnPasswordChange(val password: String) : Event()
        data class OnRepeatPasswordChange(val repeatPassword: String) : Event()
        data object BackButtonClicked : Event()
        data object RegistrationButtonClicked : Event()
    }

    data class State(
        val username: String,
        val password: String,
        val repeatPassword: String
    ) : ViewState

    sealed class Effect : ViewSideEffect {
        sealed class Navigation : Effect() {
            data object Back : Navigation()
            data object ToMain : Navigation()
        }
    }
}