package com.example.devhub.ui.feature.singup

import com.example.devhub.ui.base.BaseViewModel

class SignUpViewModel : BaseViewModel<SignUpContract.Event, SignUpContract.State, SignUpContract.Effect>() {

    override fun setInitialState() = SignUpContract.State (
        username = "",
        password = "",
        repeatPassword = ""
    )

    override fun handleEvents(event: SignUpContract.Event) {
        when (event) {
            is SignUpContract.Event.OnPasswordChange -> setState { copy(password = event.password) }
            is SignUpContract.Event.OnRepeatPasswordChange -> setState { copy(repeatPassword = event.repeatPassword) }
            is SignUpContract.Event.OnUsernameChange -> setState { copy(username = event.username) }
            is SignUpContract.Event.BackButtonClicked -> setEffect { SignUpContract.Effect.Navigation.Back }
            is SignUpContract.Event.RegistrationButtonClicked -> setEffect { SignUpContract.Effect.Navigation.ToMain }
        }
    }
}