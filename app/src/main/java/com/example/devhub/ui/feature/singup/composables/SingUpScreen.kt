package com.example.devhub.ui.feature.singup.composables

import android.annotation.SuppressLint
import androidx.compose.foundation.clickable
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.devhub.R
import com.example.devhub.ui.base.SIDE_EFFECT_KEY
import com.example.devhub.ui.feature.login.LoginContract
import com.example.devhub.ui.feature.login.composables.CustomButton
import com.example.devhub.ui.feature.login.composables.CustomOutlinedField
import com.example.devhub.ui.feature.singup.SignUpContract
import com.example.devhub.ui.theme.DarkerGreen
import com.example.devhub.ui.theme.LightGreen
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun SignUpScreen(
    state: SignUpContract.State,
    effectFlow: Flow<SignUpContract.Effect>?,
    onEventSent: (event: SignUpContract.Event) -> Unit,
    onNavigationRequest: (SignUpContract.Effect.Navigation) -> Unit
) {

    LaunchedEffect(SIDE_EFFECT_KEY) {
        effectFlow?.onEach { effect ->
            when (effect) {
                is SignUpContract.Effect.Navigation.Back -> {
                    onNavigationRequest(SignUpContract.Effect.Navigation.Back)
                }
                is SignUpContract.Effect.Navigation.ToMain -> {
                    onNavigationRequest(SignUpContract.Effect.Navigation.ToMain)
                }
            }
        }?.collect()
    }

    Scaffold {
        SignUpFields(
            state, onEventSent,
            onBackButtonClicked = { onEventSent(SignUpContract.Event.BackButtonClicked) },
            onRegistrationButtonClicked = { onEventSent(SignUpContract.Event.RegistrationButtonClicked) }
        )
    }
}

@Composable
fun SignUpFields(
    state: SignUpContract.State,
    onEventSent: (event: SignUpContract.Event) -> Unit,
    modifier: Modifier = Modifier,
    onBackButtonClicked: () -> Unit,
    onRegistrationButtonClicked: () -> Unit
) {

    val isDark = isSystemInDarkTheme()

    Column(
        modifier = modifier
            .fillMaxSize()
            .padding(horizontal = 20.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            modifier = Modifier.fillMaxWidth(),
            text = stringResource(R.string.sign_up_label),
            fontSize = 32.sp,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center
        )
        Spacer(modifier = Modifier.height(96.dp))
        CustomOutlinedField(
            text = state.username,
            placeholder = stringResource(R.string.textField_label_email),
            onTextChange = { onEventSent(SignUpContract.Event.OnUsernameChange(it)) },
            onTextClear = { onEventSent(SignUpContract.Event.OnUsernameChange("")) }
        )
        Spacer(modifier = Modifier.height(20.dp))
        CustomOutlinedField(
            text = state.password,
            placeholder = stringResource(R.string.textField_label_password),
            isPassword = true,
            onTextChange = { onEventSent(SignUpContract.Event.OnPasswordChange(it)) }
        )
        Spacer(modifier = Modifier.height(20.dp))
        CustomOutlinedField(
            text = state.repeatPassword,
            placeholder = stringResource(R.string.repeat_password),
            isPassword = true,
            onTextChange = { onEventSent(SignUpContract.Event.OnRepeatPasswordChange(it)) }
        )
        Spacer(modifier = Modifier.height(80.dp))
        CustomButton(
            text = stringResource(R.string.btn_label_signup),
            modifier = Modifier
                .fillMaxWidth(),
            onClick = onRegistrationButtonClicked
        )
        Spacer(modifier = Modifier.height(24.dp))
        Row(
            horizontalArrangement = Arrangement.spacedBy(4.dp)
        ) {
            Text(text = stringResource(R.string.already_have_an_account))
            Text(
                text = "Login",
                modifier = Modifier
                    .clickable(onClick = onBackButtonClicked),
                color = if (isDark) LightGreen else DarkerGreen,
                fontWeight = FontWeight.Bold
            )
        }
    }
}