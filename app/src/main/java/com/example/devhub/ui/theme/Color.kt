package com.example.devhub.ui.theme

import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color

val TextColor = Color(0xFF343434)
val White = Color(0xFFFFFFFF)
val Blue = Color(0xFF289BF0)
val Purple = Color(0xFF713AC0)
val LightGreen = Color(0xFF00A49B)
val DarkerGreen = Color(0xFF026773)
val DarkGreen = Color(0xFF092C4E)
val SuccessColor = Color(0xFF22C55E)
val ErrorColor = Color(0xFFDC2626)

val BluePurpleGradient = Brush.linearGradient(listOf(Blue, Purple))
val DarkGreenGradient = Brush.linearGradient(listOf(LightGreen, DarkerGreen, DarkGreen))