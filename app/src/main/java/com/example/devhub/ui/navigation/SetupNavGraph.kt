package com.example.devhub.ui.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.devhub.ui.home.HomeScreen
import com.example.devhub.ui.navigation.route.authNavGraph
import com.example.devhub.util.Graph

@Composable
fun SetupNavGraph(
    navController: NavHostController
) {
    NavHost(
        navController = navController,
        route = Graph.ROOT_GRAPH_ROUTE,
        startDestination = Graph.AUTH_GRAPH_ROUTE
    ) {
        authNavGraph(navController)
        composable(Graph.HOME_GRAPH_ROUTE) {
            HomeScreen()
        }
    }
}