package com.example.devhub.ui.feature.hotblog

import com.example.devhub.ui.base.BaseViewModel
import com.example.devhub.ui.feature.hotblog.composables.HotBlogContract
import java.time.LocalDateTime


class HotBlogViewModel : BaseViewModel<HotBlogContract.Event, HotBlogContract.State, HotBlogContract.Effect>() {
    override fun setInitialState() = HotBlogContract.State (
        articles = emptyList(),
        isLoading = true,
        isError = false
    )

    override fun handleEvents(event: HotBlogContract.Event) {
        when (event) {
            is HotBlogContract.Event.OnItemClicked -> TODO()
        }
    }

}

data class Article(
    val id: Int,
    val title: String,
    val description: String,
    val img: Int,
    val likes: Int,
    val dislikes: Int,
    val createAt: LocalDateTime,
    val user: User
)

data class User(
    val avatar: Int,
    val nickname: String
)