package com.example.devhub.ui.feature.login

import com.example.devhub.ui.base.ViewEvent
import com.example.devhub.ui.base.ViewSideEffect
import com.example.devhub.ui.base.ViewState

class LoginContract {

    sealed class Event : ViewEvent {
        data class OnEmailChange(val email: String) : Event()
        data class OnPasswordChange(val password: String) : Event()
        data object OnSignUpButtonClicked : Event()
        data object OnMainButtonClicked : Event()
    }

    data class State(
        val email: String,
        val password: String
    ) : ViewState

    sealed class Effect : ViewSideEffect {
        sealed class Navigation : Effect() {
            data object ToMain : Navigation()
            data object ToSighUp : Navigation()
        }
    }
}