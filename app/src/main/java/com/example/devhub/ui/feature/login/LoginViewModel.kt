package com.example.devhub.ui.feature.login

import com.example.devhub.ui.base.BaseViewModel

class LoginViewModel : BaseViewModel<LoginContract.Event, LoginContract.State, LoginContract.Effect>() {
    override fun setInitialState() = LoginContract.State (
        email = "",
        password = ""
    )

    override fun handleEvents(event: LoginContract.Event) {
        when (event) {
            is LoginContract.Event.OnEmailChange -> setState { copy(email = event.email) }
            is LoginContract.Event.OnPasswordChange -> setState { copy(password = event.password) }
            is LoginContract.Event.OnMainButtonClicked -> setEffect { LoginContract.Effect.Navigation.ToMain }
            is LoginContract.Event.OnSignUpButtonClicked -> setEffect { LoginContract.Effect.Navigation.ToSighUp }
        }
    }
}