package com.example.devhub.ui.navigation.route

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.devhub.R
import com.example.devhub.ui.feature.aboutus.AboutUsScreen
import com.example.devhub.ui.feature.hotblog.composables.HotBlogsScreen
import com.example.devhub.ui.feature.myfeed.MyFeedScreen
import com.example.devhub.ui.feature.subscription.SubscriptionScreen
import com.example.devhub.util.Graph

@Composable
fun HomeNavGraph(
    navController: NavHostController
) {
    NavHost(
        navController = navController,
        route = Graph.HOME_GRAPH_ROUTE,
        startDestination = MainScreen.HotBlog.route
    ) {
        composable(MainScreen.HotBlog.route) {
            HotBlogsScreen()
        }
        composable(MainScreen.MyFeed.route) {
            MyFeedScreen()
        }
        composable(MainScreen.Subscription.route) {
            SubscriptionScreen()
        }
        composable(MainScreen.AboutUs.route) {
            AboutUsScreen()
        }
    }
}

private sealed class MainScreen(val route: String) {
    data object HotBlog : MainScreen("hotblog")
    data object MyFeed : MainScreen("myfeed")
    data object Subscription : MainScreen("subscription")
    data object AboutUs : MainScreen("aboutus")
}

sealed class BottomBarItem(
    val route: String,
    val icon: Int,
    val title: String
) {
    data object HotBlog : BottomBarItem(MainScreen.HotBlog.route, R.drawable.ic_flame, "Горячее")
    data object MyFeed : BottomBarItem(MainScreen.MyFeed.route, R.drawable.ic_feed, "Моя лента")
    data object Subscription : BottomBarItem(MainScreen.Subscription.route, R.drawable.ic_user_plus, "Подписки")
    data object AboutUs : BottomBarItem(MainScreen.AboutUs.route, R.drawable.ic_users, "О нас")

    object Items {
        val list = listOf(
            HotBlog, MyFeed, Subscription, AboutUs
        )
    }
}
