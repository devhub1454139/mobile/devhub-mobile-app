package com.example.devhub

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DevHubApplication : Application()